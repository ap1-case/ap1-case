# AP Case Challenge
Welcome to the AP Case Challenge. This repository includes all resources necessary to participate in the challenge. You can clone this repository and start implementing your strategy.
If you do not have python installed on your computer you can also checkout the [binder](https://mybinder.org/v2/gl/ap1-case%2Fap1-case/master) that hosts this repository.


# Challenge
This participants in this challenge will implement and tune a proposed FX trading strategy using performance of equity indices as input.

The challenge can be solved individually or in groups of maximum 4 members. The best entries will have the opportunity to present their strategy during the case night as well as be given the chance to write their master thesis with one of the AP-funds.

In order to participate in the challenge, please the use this repository and construct your implementation of the strategy. 


# Strategy
The strategy we would like the participants to implement uses performance of major equity indices to predict the return of different currencies. The reason we can expect the equity market to have predictive power in the FX-market is due to the institutional use of currency hedging.


### Currency Hedging
In very simple terms, Currency Hedging is the act of entering into a financial contract in order to protect against unexpected, expected or anticipated changes in currency exchange rates.

Currency hedging is used by financial investors and businesses to eliminate risks they encounter when conducting business internationally. Hedging can be likened to an insurance policy that limits the impact of foreign exchange risk. Hedging can be accomplished by purchasing or booking different types of contracts that are designed to achieve specific goals. All these contracts have in common that they entail a purchase of on currency and a sell in another.

If we can find a strategy that can predict the net effect of these hedges we can use this to systematically invest in the FX market.

A lot of institutional investors will rebalance their currency hedges at fixed intervals. If the asset managers' portfolios have had an increase in a specific currency exposure they will most likely sell that currency when the FX hedge is rebalanced (and vice versa). The effect of many institutional investors selling/buying a specific currency will drive the currency price down/up.

##### An example:
Swedish institutional investor have an FX benchmark of 50% EUR and 50% USD. The portfolio consists, at month start, of 50% Equity denominated in EUR and 50% Bond denominated in USD. I.e. the investors is matching the benchmark FX exposure.

The investor will rebalance the FX hedges to match the benchmark of 50%/50% EUR/USD exposure monthly.

After one month the Equity market have outperformed the Bond market. The portfolio now consists of 55% Equity (denominated in EUR) and 45% Bonds (denominated in USD).

In order to match the FX exposure the investor will sell EUR and buy USD.

### Month End FX Rebalancing Strategy

The main idea is to calculate the performance of the equity indexes over some time horizon. Then rank the indexes based on relative performance. Finally, we will go long the quotation currencies of the equity indexes that have had a negative relative performance, and vice versa.

The strategy have a lot of different parameters that will decide the performance of the strategy. For instance:
* The frequency of when we decide to take our positions. Could be daily, weekly, monthly etc.
* The lookback window we use to measure the relative performance of the equity indices.
* The weighting method between the different currencies.

All these parameters should be optimized by the participants and preferably some analysis should be perform to motivate the final selection.

# Resources
In this repository we have included some resources that can help you implement the strategy.
### Notebooks
Please go through the two notebooks in the repository to get a further introduction to strategy.

* Tutorial which introduces data manipulation/visualization with pandas as well as supp.py which is created to ease the coding needed for this challenge.

* Challenge which introduce supp.py and on what format the strategy should be implmented.


### Data 
In the folder /data we have collected a data set for the biggest currencies and their associated indexes.

We have also included a notebook that retrieves said data from Yahoo Finance.

You can use other data if you would like.


# Solution 
Together with the implemented strategy (see the Challenge notebook) participants are encouraged to perform statistical- and sensitivity analysis that can help evaluate the strategy. Your code and supportive analysis should be emailed to (fredrik.nylund@ap1.se) at the latest the 4th November.

# Questions 
Please email fredrik.nylund@ap1.se if you have any questions regarding the repository or the case challenge in general
