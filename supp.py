
import pandas as pd
import numpy as np
from pandas.core.base import PandasObject
import itertools

import datetime as dt
from tqdm.autonotebook import tqdm
import matplotlib.pyplot as plt

from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

def to_log_returns(prices):
    """
    Calculates the log returns of a price series.

    Formula is: ln(p1/p0)

    Args:
        * prices: Expects a price series

    """
    # Drop rows where everything is na
    prices.dropna(how='all', inplace=True)
    # Forward fill gaps
    prices.fillna(method='ffill', inplace=True)
    # Shift by one and calculate log
    lgrt=np.log(prices / prices.shift(1))
    # Replace zeros with nan and remove rows with na
    lgrt=lgrt.replace(0, np.nan).dropna(how='all')
    return lgrt



def to_index(series, start=100):
    """
    Calculates the price index of a price or log-return series.

    Formula is: 

    Args:
        * series: Expects a price series or returns

    """
    if np.nanmin(series)>0:
        returns=to_log_returns(series)
    else:
        returns=series
    return np.exp((returns.replace(to_replace=np.nan, value=0)).cumsum()) * start

# Cast Log Returns and Index to panda frames
PandasObject.to_log_returns = to_log_returns
PandasObject.to_index = to_index
PandasObject.to_price_index = to_index



def correlations(df, lookback=252):
    """
    Input dataframe with log returns and the lookback window to get the full rolling correlation
    """
    comb=list(itertools.combinations(df.columns, 2))
    corr_df_array=[]
    for i in comb:
        # Reduce to smaller df, this will effectively select indexes where both timeseries have data points 
        red_df=df[list(i)].dropna()
        corr_df_array.append(red_df[i[0]].rolling(lookback).corr(red_df[i[1]]))
    corr_df=pd.concat(corr_df_array, axis=1)
    
    corr_df.columns=[i[0]+"-"+i[1] for i in comb]
    return corr_df

# Cast Corrlations to pandas
PandasObject.correlations = correlations

import sys, os

class HiddenPrints:
    def __enter__(self):
        self._original_stdout = sys.stdout
        sys.stdout = open(os.devnull, 'w')

    def __exit__(self, exc_type, exc_val, exc_tb):
        sys.stdout.close()
        sys.stdout = self._original_stdout
        

datasets={
    'FTSE' : {
        "index" : 'FTSE (GBP)',
        "benchmark" : 'SPX (USD)',
        "fx" : 'GBP/USD',
    },
    'NIKKEI' : {
        "index" : 'Nikkei (JPY)',
        "benchmark" : 'SPX (USD)',
        "fx" : 'JPY/USD',
    },
    'DAX' : {
        "index" : 'DAX (EUR)',
        "benchmark" : 'SPX (USD)',
        "fx" : 'EUR/USD',
    },
    'OMX' : {
        "index" : 'OMX (SEK)',
        "benchmark" : 'SPX (USD)',
        "fx" : 'SEK/USD',
    }
}        
        
    
    


def load_data(field="ADJUSTED_CLOSE_PRICES"):
    """
    Loads data from 
    """
    return pd.read_pickle('./data/'+field+'.p').to_log_returns()


class Strategy:
    def __init__(self):
        pass
    
    def take_position(self, data, date, prev_date):
        """
        indata  -  as a dataframe with columns: I (Index), BM (Benchmark), FX (Exchange Rate)
        date - date of when the positions will be taken
        prev_date - previous date when signal was generated
        
        Has to return a float between 1 and -1
        """
        raise NotImplementedError
        
    def _load_data(self, index="OMX"):
        """
        Loads data for specified index
        """
        df=load_data()
        ind=df[datasets[index]['index']]
        bm=df[datasets[index]['benchmark']]
        fx=df[datasets[index]['fx']]
        self.data=pd.concat([ind, bm, fx],axis=1).dropna(how='all')
        self.data.columns=["I", "BM", "FX"]
        print("Loaded data for", index)
        
        
    def _test_predict(self, shift=3):
        """
        Generates predictions
        """
        # Apply shift and calculate monthly dates
        tds=self.data.resample('M').last().resample('1D').last()
        ind=[t for t in self.data.index if t in tds.index]
        tds=tds.loc[ind,:].shift(-shift).dropna().index
        # Create predictions and long only comparision
        predictions={}
        alternative={}
        print("Generating predictions")
        c=0
        for td in tqdm(tds[1:]):
            # Separates to available data
            available_data=self.data[:td-dt.timedelta(days=1)]
            # Saves prediction
            predictions[td]=self.take_position(available_data, td, tds[c])
            alternative[td]=1
            c+=1
        # Save predictions and long only comparision
        self.predictions=pd.DataFrame.from_dict({'predictions' : predictions})
        self.alternative=pd.DataFrame.from_dict({'predictions' : alternative})
        print("Predictions are done")
       
    def _test_evaluate(self, holding, key):
        """
        Evaluates
        """
        # Format actual strategy
        pr0=self.predictions.resample('D').last()
        ind=[d for d in self.data['FX'].dropna().index if d in pr0.index]
        pr0=pr0.loc[ind, :]
        pr=pd.concat([pr0.shift(i) for i in range(holding)], axis=1).sum(axis=1).dropna().to_frame()
        pr.columns=['predictions']
        df=(self.data['FX']*pr['predictions'])
        
        # Format long-only bet
        pr0=self.alternative.resample('D').last()
        ind=[d for d in self.data['FX'].dropna().index if d in pr0.index]
        pr0=pr0.loc[ind, :]
        pr=pd.concat([pr0.shift(i) for i in range(holding)], axis=1).sum(axis=1).to_frame()
        pr.columns=['predictions']
        df2=(self.data['FX']*pr['predictions'])
        
        # Plot results
        plt.figure(figsize=(20, 10))
        plt.plot(df.to_index(),label = "Strategy")
        plt.plot(df2.to_index(), label = "Long-Only")
        plt.title("Strategy for "+key)
        plt.legend()
        plt.show()
        
        self.result=df
        self.result.columns=[key]

    def test(self, index="OMX", shift=3, holding=5):
        """
        Used to test strategies
        index="All" - All (OMX/DAX/FTSE/NIKKEI) will run all strategies
        shift=3 - How many days 
        holding=3 - How many days to hold position
        """
        if index=="All":
            # If all return test results for all indexes, returns dictionary
            results={}
            for k in datasets.keys():
                # Performs test
                results[k]=self.test(k, shift)
            return results
        else:
            # Load data
            self._load_data(index)
            # Define test
            self._test_predict(shift)
            self._test_evaluate(holding, index)
            print("")
            return self.result
        
    def sample_indata(self):
        # Load some data if missing
        if not hasattr(self,'data'):
            self._load_data()
                
        d=np.random.randint(len(self.data))
        date=self.data.iloc[:d+1, :].index[-1]
        data=self.data.iloc[:d, :]
        print("Date: ", date.strftime('%Y%m%d'), ", previous date is not accurate")
        return {"indata" : data, "date" : date, "prev_date" : (date-dt.timedelta(days=30))}

    
        
print("Supp lib imported")





